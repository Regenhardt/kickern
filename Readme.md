# Spielplangenerator

Diese Web App soll einen Spielplan f�r ein Ligaspiel zwischen zwei Kickerteams generieren.  
Es soll die Aufstellung eines Teams f�r alle Spiele dieser Begegnung generiert werden.

## Regeln

- Das Team besteht aus mindestens 4 Spielern
- Jeder Spieler darf h�chstens zwei Einzel und zwei Doppel spielen
- Jeder Spieler darf h�chstens ein Doppel mit jedem anderen Spieler spielen

## Pr�ferenzen

- Ein Spieler kann vorne oder hinten bevorzugen, oder keine Pr�ferenz haben
- Ein Spieler kann einen oder mehrere Partner bevorzugen
- Ein Spieler bevorzugt Einzel oder Goalie statt Einzel

## Struktur

Ein Spielplan besteht aus folgenden Spielen:

- Einzel
- Einzel
- Doppel
- Entscheidung:
  - Bei h�chstens 4 Spielern:
    - Einzel
    - Einzel
  - Bei mehr als 4 Spielern:
    - Doppel oder
    - zwei Einzel
- Doppel  

**Halbzeit**

- Einzel oder Goalie
- Einzel oder Goalie
- Doppel
- Einzel
- Einzel
- Doppel

## App Design

- Mobile first, responsive
  - Smartphone mit Tabs
  - Tablet/Desktop ggf. geteilte Ansicht
- Tab-Navigation unten
  - Symbole
  - Seitentitel bei aktiver Seite
- Seiten/Tabs:  
  - Spielplan
    - Validiert Konfiguration, ggf. Fehlermeldung anzeigen
    - Generiert Spielplan auf Knopfdruck
    - Generierter Spielplan in localStorage speichern
    - bei Einzel/Goalie Pr�ferenz des jeweils gew�hlten Spielers markieren
  - Konfiguration
    - Konfiguriert Spieler und deren Pr�ferenzen
    - Konfiguration in localStorage speichern
  - Info
    - Aktuelle Informationen
    - ggf. sp�ter falls es zu gro� wird:
      - Anleitung
      - Versionshinweise
      - Kontakt
      - Impressum
      - Datenschutz