using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Spielplangenerator;
using Spielplangenerator.Services;
using Spielplangenerator.Spielplan;
using Spielplangenerator.UiComponents.Toast;
using Toolbelt.Blazor.Extensions.DependencyInjection;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddPWAUpdater();
builder.Services.AddLogging(
    loggingBuilder => loggingBuilder.SetMinimumLevel(
        builder.Configuration.GetValue<LogLevel>("Logging:LogLevel:Default")));
builder.Services
    .AddSingleton<IStorage, LocalStorage>()
    .AddSingleton<ToastService>()
    .AddSingleton<AppState>()
    .AddTransient<SpielplanService>();

await builder.Build().RunAsync();
