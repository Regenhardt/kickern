namespace Spielplangenerator.Services;
using System.ComponentModel.DataAnnotations;

public class AllowNullAttribute : ValidationAttribute
{
    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        return ValidationResult.Success;
    }
}