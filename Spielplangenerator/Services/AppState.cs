﻿namespace Spielplangenerator.Services;

using Spielplan;
using Spielplangenerator.Konfiguration;

public class AppState
{
    const string EinstellungenKey = "Spielplangenerator-Einstellungen";
    const string SpielplanKey = "Spielplangenerator-Spielplan";
    public event Action? OnInitialized;
    private readonly IStorage storage;
    public Einstellungen Einstellungen { get; set; } = new();
    public Begegnung? Spielplan { get; set; }

    public AppState(IStorage storage)
    {
        this.storage = storage;
        Task.Run(async () =>
        {
            Einstellungen = await storage.Get<Einstellungen>(EinstellungenKey) ?? new();
            Spielplan = await storage.Get<Begegnung>(SpielplanKey);
            OnInitialized?.Invoke();
        });
    }

    public async Task SpeichereEinstellungen()
    {
        await storage.Set(EinstellungenKey, Einstellungen);
    }

    public void LoescheEinstellungen()
    {
        Einstellungen = new();
        storage.Remove(EinstellungenKey);
    }

    public void SpeichereSpielplan()
    {
        storage.Set(SpielplanKey, Spielplan);
    }

    public async Task LoescheSpielplan()
    {
        await storage.Remove(SpielplanKey);
    }
}
