﻿namespace Spielplangenerator.Services;

using Microsoft.JSInterop;
using System.Text.Json;
using System.Text.Json.Serialization;

public class LocalStorage(IJSRuntime jSRuntime) : IStorage
{
    public async Task Set<T>(string key, T value)
    {
        await jSRuntime.InvokeVoidAsync("localStorage.setItem", key, JsonSerializer.Serialize(value));
    }

    public async Task<T?> Get<T>(string key) where T : class
    {
        var str = await jSRuntime.InvokeAsync<string?>("localStorage.getItem", key);
        return str == null ? null : JsonSerializer.Deserialize<T>(str);
    }

    public async Task Remove(string key)
    {
        await jSRuntime.InvokeVoidAsync("localStorage.removeItem", key);
    }

    public async Task<T?> GetStruct<T>(string key) where T : struct
    {
        return await jSRuntime.InvokeAsync<T?>("localStorage.getItem", key);
    }
}

