﻿namespace Spielplangenerator.Services;

public interface IStorage
{
    Task Set<T>(string key, T value);
    Task<T?> Get<T>(string key) where T : class;
    Task<T?> GetStruct<T>(string key) where T : struct;
    Task Remove(string key);
}
