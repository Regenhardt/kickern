﻿namespace Spielplangenerator.Konfiguration;

using System.ComponentModel.DataAnnotations;

public class Einstellungen : IValidatableObject
{
    [Required(ErrorMessage = "Spieler eingeben")]
    public HashSet<Player> Spieler { get; set; } = [];
    public bool DoppelStattEinzel { get; set; }

    public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
    {
        if (Spieler.Count < 4)
        {
            yield return new ValidationResult("Mindestens vier Spieler erforderlich", new[] { nameof(Spieler) });
        }

        if(Spieler.Count < 5 && DoppelStattEinzel)
        {
            yield return new ValidationResult("Mindestens fünf Spieler erforderlich für Doppel", new[] { nameof(Spieler) });
        }

        foreach(var spieler in Spieler)
        {
            var results = new List<ValidationResult>();
            Validator.TryValidateObject(spieler, new ValidationContext(spieler), results);
            foreach (var result in results)
            {
                yield return result;
            }
        }
    }
}
