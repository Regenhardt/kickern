﻿namespace Spielplangenerator.UiComponents.Toast;

using System.Diagnostics.CodeAnalysis;
using System.Timers;

public class ToastService : IDisposable
{
    public event Action<string, ToastLevel>? OnShow;
    public event Action? OnHide;
    private Timer? countdown;

    public void ShowToast(string message, ToastLevel level)
    {
        OnShow?.Invoke(message, level);
        StartCountdown();
    }

    private void StartCountdown()
    {
        SetCountdown();
        if (countdown.Enabled)
        {
            countdown.Stop();
            countdown.Start();
        }
        else
        {
            countdown.Start();
        }
    }

    [MemberNotNull(nameof(countdown))]
    private void SetCountdown()
    {
        if (countdown == null)
        {
            countdown = new Timer(5000);
            countdown.Elapsed += HideToast;
            countdown.AutoReset = false;
        }
    }
    private void HideToast(object? source, ElapsedEventArgs args)
    {
        OnHide?.Invoke();
    }
    public void Dispose()
    {
        countdown?.Dispose();
    }
}
