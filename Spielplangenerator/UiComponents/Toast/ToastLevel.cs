﻿namespace Spielplangenerator.UiComponents.Toast;

public enum ToastLevel
{
    Info,
    Success,
    Warning,
    Error
}
