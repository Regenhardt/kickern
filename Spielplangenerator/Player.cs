﻿namespace Spielplangenerator;

using Spielplan;
using System.ComponentModel.DataAnnotations;
using Services;

public record Player
{
    [MinLength(1, ErrorMessage = "Spielername eingeben")]
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// Welche Position, vorne oder hinten, der Spieler bevorzugt, oder null, wenn er keine Präferenz hat.
    /// </summary>
    [AllowNull]
    public Position? Position { get; set; } = null;

    /// <summary>
    /// Ob der Spieler bei den Einzel nach der Halbzeitpause Goalie oder normales Einzel bevorzugt.
    /// </summary>
    [AllowedValues(Spielmodus.Einzel, Spielmodus.Goalie, ErrorMessage = "Einzelspielermodus darf nur Einzel oder Goalie sein.")]
    public Spielmodus Einzelspielmodus { get; set; }

    /// <summary>
    /// Ob der Spieler möglichst nach der ersten Halbzeit los möchte.
    /// </summary>
    public bool WillSchnellWeg { get; set; }

    /// <summary>
    /// Mit welchen Spielern dieser Spieler wenn möglich spielen möchte.
    /// </summary>
    public HashSet<string> BevorzugteMitspieler { get; set; } = new();
}