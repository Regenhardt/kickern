﻿namespace Spielplangenerator.Spielplan;

public class Begegnung(List<Spiel> ersteHalbzeit, List<Spiel> zweiteHalbzeit)
{
    public List<Spiel> ErsteHalbzeit { get; set; } = ersteHalbzeit;
    public List<Spiel> ZweiteHalbzeit { get; set; } = zweiteHalbzeit;
    public Ergebnis Ergebnis => new(
        ErsteHalbzeit.Sum(spiel => spiel.Ergebnis.Wir) + ZweiteHalbzeit.Sum(spiel => spiel.Ergebnis.Wir),
        ErsteHalbzeit.Sum(spiel => spiel.Ergebnis.Sie) + ZweiteHalbzeit.Sum(spiel => spiel.Ergebnis.Sie)
        );
}
