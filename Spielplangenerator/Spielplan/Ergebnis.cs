﻿namespace Spielplangenerator.Spielplan;

public class Ergebnis(int wir, int sie)
{
    public int Wir { get; set; }
    public int Sie { get; set; }

    public string Stand() => $"{wir}:{sie}";

    public string BestimmeSpielErgebnis() => 
        wir > sie ? "Sieg"
        : wir == sie ? "Unentschieden"
        : "Niederlage";
}
