﻿namespace Spielplangenerator.Spielplan;

public class InvalidSetupException(List<Spiel> bisherigeSpiele, Spielmodus verlangtesSpiel, HashSet<Player> letzteSpieler, string? message) : Exception(message)
{
    public List<Spiel> BisherigeSpiele { get; } = bisherigeSpiele;
    public Spielmodus VerlangtesSpiel { get; } = verlangtesSpiel;
    public HashSet<Player> LetzteSpieler { get; } = letzteSpieler;
}
