﻿namespace Spielplangenerator.Spielplan;

public enum Spielmodus
{
    Einzel,
    Doppel,
    Goalie
}
