﻿namespace Spielplangenerator.Spielplan;

using Services;
using System.ComponentModel.DataAnnotations;

public class SpielplanService(AppState state, ILogger<SpielplanService> logger)
{
    private List<Spiel> ersteHalbzeit = new();
    private List<Spiel> zweiteHalbzeit = new();
    private Dictionary<Player, int> anzahlEinzel = new();
    private Dictionary<Player, int> anzahlDoppel = new();

    public void GeneriereSpielplan()
    {
        var validation = state.Einstellungen.Validate(new(state.Einstellungen)).ToList();
        if (validation.Count != 0)
        {
            var errorMessage = string.Join(". ", validation.Select(r => r.ErrorMessage));
            throw new ValidationException(string.Join(". ", ["Einstellungen sind nicht valide", errorMessage]));
        }

        ersteHalbzeit = new();
        zweiteHalbzeit = new();
        anzahlEinzel = new();
        anzahlDoppel = new();
        var verfuegbarEinzel = state.Einstellungen.Spieler.ToHashSet();
        var verfuegbarDoppel = state.Einstellungen.Spieler.ToHashSet();

        // 1. Halbzeit
        logger.LogInformation("1.HZ");
        GeneriereEinzel(verfuegbarEinzel, false);
        GeneriereEinzel(verfuegbarEinzel, false);
        GeneriereDoppel(verfuegbarDoppel, false);
        if (verfuegbarEinzel.Count <= 4 || !state.Einstellungen.DoppelStattEinzel)
        {
            GeneriereEinzel(verfuegbarEinzel, false);
            GeneriereEinzel(verfuegbarEinzel, false);
        }
        else
        {
            GeneriereDoppel(verfuegbarDoppel, false);
        }
        GeneriereDoppel(verfuegbarDoppel, false);

        // Halbzeitpause

        // 2. Halbzeit
        logger.LogInformation("2.HZ");
        GeneriereEinzel(verfuegbarEinzel, true, true);
        GeneriereEinzel(verfuegbarEinzel, true, true);
        GeneriereDoppel(verfuegbarDoppel, true);
        GeneriereEinzel(verfuegbarEinzel, true);
        GeneriereEinzel(verfuegbarEinzel, true);
        GeneriereDoppel(verfuegbarDoppel, true);

        logger.LogInformation("---");


        state.Spielplan = new(ersteHalbzeit, zweiteHalbzeit);
    }

    /// <summary>
    /// Generiert ein neues Einzel und fügt es dem Spielplan hinzu.
    /// </summary>
    /// <remarks>
    /// Jeder Spieler würfelt und der Spieler mit der höchsten Zahl spielt das Spiel.<br />
    /// Basiswahrscheinlichkeit 100, also random(100) für jeden Spieler.<br />
    /// Hat ein Spieler im vorherigen Spiel gespielt, wird dessen Spielwahrscheinlichkeit um 20 verringert.<br />
    /// Hat ein Spieler im Spiel davor gespielt, wird dessen Spielwahrscheinlichkeit um 10 verringert.<br />
    /// Für jedes weitere Einzel, das ein Spieler bereits gespielt hat, wird dessen Spielwahrscheinlichkeit um 5 verringert.<br />
    /// Soll das Spiel für die erste Halbzeit generiert werden und hat ein Spieler die Präferenz, früh gehen zu wollen, wird dessen Spielwahrscheinlichkeit um 30 erhöht.<br />
    /// Hat ein Spieler die Einzelspielerpräferenz Goalie, wird dessen Spielwahrscheinlichkeit bei nicht-Goalie-Spielen um 100 verringert und bei Goalie-Spielen um 1000 erhöht um sicherzugehen, dass diese Spieler beim Goalie gewählt werden.
    /// </remarks>
    /// <param name="verfuegbar">Liste mit noch verfügbaren Spielern.</param>
    /// <param name="fuerZweiteHalbzeit">Ob das Spiel für die zweite Halbzeit generiert werden soll.</param>
    /// <param name="goalieMoeglich">Ob dieses Spiel auch als Goalie gespielt werden kann. Ist dies gesetzt, wird bei Erstellung die Präferenz des gewählten Spielers genutzt.</param>
    private void GeneriereEinzel(HashSet<Player> verfuegbar, bool fuerZweiteHalbzeit, bool goalieMoeglich = false)
    {
        logger.LogDebug("Generiere Einzel.");
        ArgumentOutOfRangeException.ThrowIfLessThan(verfuegbar.Count, 1, "Es muss mindestens 1 Spieler verfügbar sein.");

        var halbzeit = fuerZweiteHalbzeit ? zweiteHalbzeit : ersteHalbzeit;
        var alleSpiele = ersteHalbzeit.Concat(zweiteHalbzeit).ToList();

        // Wenn nur noch ein Spieler verfügbar ist, wird dieser Spieler das Spiel spielen.
        if (verfuegbar.Count == 1)
        {
            var letzteWahl = verfuegbar.Single();
            logger.LogInformation("E {Name}", letzteWahl.Name);
            halbzeit.Add(new(goalieMoeglich ? letzteWahl.Einzelspielmodus : Spielmodus.Einzel, [letzteWahl], new(0, 0)));
            UpdatePlayerCount(letzteWahl, anzahlEinzel, verfuegbar);
            logger.LogDebug("Nur noch ein Spieler verfügbar: {Name}", letzteWahl.Name);
            return;
        }

        var wahrscheinlichkeiten = new Dictionary<Player, int>();
        foreach (var spieler in verfuegbar)
        {
            var spielwahrscheinlichkeit = 100;
            if (alleSpiele.Count > 0)
            {
                var vorherigesSpiel = alleSpiele[^1];
                if (vorherigesSpiel.Spieler.Contains(spieler))
                {
                    spielwahrscheinlichkeit -= 20;
                }
                if (alleSpiele.Count > 1)
                {
                    var vorvorherigesSpiel = alleSpiele[^2];
                    if (vorvorherigesSpiel.Spieler.Contains(spieler))
                    {
                        spielwahrscheinlichkeit -= 10;
                    }

                    spielwahrscheinlichkeit -= 5 * alleSpiele.Count(s => s.Modus == Spielmodus.Einzel && s.Spieler.Contains(spieler));
                }

                if (spieler.Einzelspielmodus == Spielmodus.Goalie)
                {
                    if (goalieMoeglich && !vorherigesSpiel.Spieler.Contains(spieler))
                    {
                        spielwahrscheinlichkeit += 1000;
                    }
                    else
                    {
                        spielwahrscheinlichkeit -= 100;

                    }
                }
            }

            if(!fuerZweiteHalbzeit && spieler.WillSchnellWeg)
            {
                spielwahrscheinlichkeit += 30;
            }

            // Rutscht es ins negative, gehen wir zurück auf "möglichst nicht spielen"
            if(spielwahrscheinlichkeit < 0)
            {
                spielwahrscheinlichkeit = 1;
            }

            wahrscheinlichkeiten.Add(spieler, spielwahrscheinlichkeit);
        }

        var auswahl = SelectPlayer(wahrscheinlichkeiten, verfuegbar);

        halbzeit.Add(new Spiel(goalieMoeglich ? auswahl.Einzelspielmodus : Spielmodus.Einzel, [auswahl], new(0, 0)));
        UpdatePlayerCount(auswahl, anzahlEinzel, verfuegbar);
        logger.LogInformation("E {Name}", auswahl.Name);
    }

    /// <summary>
    /// Generiert ein neues Doppel und fügt es dem Spielplan hinzu.
    /// </summary>
    /// <remarks>
    /// <b>Erster Spieler</b><br />
    /// Jeder Spieler würfelt und der mit der höchsten Zahl ist der erste Spieler.<br />
    /// Basiswahrscheinlichkeit 100, also random(100) für jeden Spieler.<br />
    /// Hat ein Spieler im vorherigen Spiel gespielt, wird dessen Spielwahrscheinlichkeit um 20 verringert.<br />
    /// Hat ein Spieler im Spiel davor gespielt, wird dessen Spielwahrscheinlichkeit um 10 verringert.<br />
    /// Für jedes weitere Spiel, das ein Spieler bereits gespielt hat, wird dessen Spielwahrscheinlichkeit um 5 verringert.<br />
    /// Ist das Spiel für die erste Halbzeit und hat ein Spieler die Präferenz, früh gehen zu wollen, wird dessen Spielwahrscheinlichkeit um 30 erhöht.<br />
    /// Hat ein Spieler die Präferenz, mit einem Mitspieler zu spielen, der früh gehen möchte, wird dessen Spielwahrscheinlichkeit um 30 erhöht.
    /// Hat ein Spieler eben gerade in einem Doppel gespielt, wird dessen Spielwahrscheinlichkeit auf 1 gesetzt.<br />
    /// Hat ein Spieler bereits mit allen anderen verfügbaren Spielern ein Doppel gespielt, wird dessen Spielwahrscheinlichkeit auf 0 gesetzt.<br />
    /// 
    /// <b>Zweiter Spieler</b><br />
    /// Jeder andere Spieler würfelt und der mit der höchsten Zahl ist der zweite Spieler.<br />
    /// Basiswahrscheinlichkeit 100, also random(100) für jeden Spieler.<br />
    /// Hat ein Spieler im vorherigen Spiel gespielt, wird dessen Spielwahrscheinlichkeit um 20 verringert.<br />
    /// Hat ein Spieler im Spiel davor gespielt, wird dessen Spielwahrscheinlichkeit um 10 verringert.<br />
    /// Für jedes weitere Spiel, das ein Spieler bereits gespielt hat, wird dessen Spielwahrscheinlichkeit um 5 verringert.<br />
    /// Für jedes Spiel, das ein Spieler bereits mit dem ersten Spieler gespielt hat, wird dessen Spielwahrscheinlichkeit um 40 verringert.<br />
    /// Passt die präferierte Position eines Spielers nicht zu der des ersten Spielers, wird dessen Spielwahrscheinlichkeit um 30 verringert.<br />
    /// Ist das Spiel für die erste Halbzeit und hat ein Spieler die Präferenz, früh gehen zu wollen, wird dessen Spielwahrscheinlichkeit um 30 erhöht.<br />
    /// Hat ein Spieler die Präferenz, mit dem ersten Spieler zu spielen, oder andersrum, wird dessen Spielwahrscheinlichkeit um 30 erhöht.
    /// Hat ein Spieler eben gerade in einem Doppel gespielt, wird dessen Spielwahrscheinlichkeit auf 1 gesetzt.<br />
    /// Hat ein Spieler bereits mit dem ersten gewählten Spieler ein Doppel gespielt, wird dessen Spielwahrscheinlichkeit auf 0 gesetzt.<br />
    /// </remarks>
    /// <param name="verfuegbar">Liste mit noch verfügbaren Spielern.</param>
    /// <param name="fuerZweiteHalbzeit">Ob das Spiel für die zweite Halbzeit generiert werden soll.</param>
    private void GeneriereDoppel(HashSet<Player> verfuegbar, bool fuerZweiteHalbzeit)
    {
        logger.LogDebug("Generiere Doppel.");
        ArgumentOutOfRangeException.ThrowIfLessThan(verfuegbar.Count, 2, "Es müssen mindestens 2 Spieler verfügbar sein.");

        var halbzeit = fuerZweiteHalbzeit ? zweiteHalbzeit : ersteHalbzeit;
        List<Spiel> alleSpiele = [.. ersteHalbzeit, .. zweiteHalbzeit];

        bool HatVorigesDoppelGespielt(Player player) => alleSpiele.LastOrDefault(spiel => spiel.Modus == Spielmodus.Doppel)?.Spieler.Contains(player) == true;

        bool HabenGemeinsamesDoppel(Player p1, Player p2) => alleSpiele.Where(spiel => spiel.Modus == Spielmodus.Doppel).Any(spiel => spiel.Spieler.Contains(p1) && spiel.Spieler.Contains(p2));

        // Wenn nur noch zwei Spieler verfügbar sind, werden diese Spieler das Spiel spielen.
        if (verfuegbar.Count == 2)
        {
            var (p1, p2) = (verfuegbar.First(), verfuegbar.Last());
            if (HabenGemeinsamesDoppel(p1, p2))
            {
                throw new InvalidSetupException(alleSpiele, Spielmodus.Doppel, verfuegbar, "Die letzten verfügbaren Spieler haben bereits ein Doppel gespielt.");
            }

            logger.LogInformation("D {Spieler}", string.Join(", ", verfuegbar.Select(s => s.Name)));
            halbzeit.Add(new(Spielmodus.Doppel, [p1, p2], new(0, 0)));
            UpdatePlayerCount(p1, anzahlDoppel, verfuegbar);
            UpdatePlayerCount(p2, anzahlDoppel, verfuegbar);
            logger.LogDebug("Verbleibende Spieler: {Spieler}", string.Join(", ", verfuegbar.Select(s => s.Name)));
            return;
        }

        // Erster Spieler
        var wahrscheinlichkeiten = new Dictionary<Player, int>();
        foreach (var spieler in verfuegbar)
        {
            var spielwahrscheinlichkeit = 100;
            if (alleSpiele.Count > 0)
            {
                var vorherigesSpiel = alleSpiele[^1];
                if (vorherigesSpiel.Spieler.Contains(spieler))
                {
                    spielwahrscheinlichkeit -= 20;
                }
                if (alleSpiele.Count > 1)
                {
                    var vorvorherigesSpiel = alleSpiele[^2];
                    if (vorvorherigesSpiel.Spieler.Contains(spieler))
                    {
                        spielwahrscheinlichkeit -= 10;
                    }

                    spielwahrscheinlichkeit -= 5 * alleSpiele.Count(s => s.Modus == Spielmodus.Doppel && s.Spieler.Contains(spieler));
                }
            }

            if (!fuerZweiteHalbzeit && spieler.WillSchnellWeg)
            {
                spielwahrscheinlichkeit += 30;
            }

            if(!fuerZweiteHalbzeit && 
                (spieler.BevorzugteMitspieler.Any(s => verfuegbar.Single(ms => ms.Name == s).WillSchnellWeg) ||
                verfuegbar.Any(ms => ms.WillSchnellWeg && ms.BevorzugteMitspieler.Contains(spieler.Name))))
            {
                spielwahrscheinlichkeit += 30;
            }

            // Hat der Spieler gerade in einem Doppel gespielt, soll er nur in Ausnahmefällen direkt wieder spielen
            if (HatVorigesDoppelGespielt(spieler))
            {
                spielwahrscheinlichkeit = 1;
            }

            // Hat der Spieler bereits mit allen anderen verfügbaren Spielern ein Doppel gespielt, darf er kein Doppel mehr spielen
            if (verfuegbar.Where(ms => ms != spieler).All(ms => HabenGemeinsamesDoppel(spieler, ms)))
            {
                spielwahrscheinlichkeit = 0;
            }

            wahrscheinlichkeiten.Add(spieler, spielwahrscheinlichkeit);
        }

        var ersterSpieler = SelectPlayer(wahrscheinlichkeiten, verfuegbar);

        // Zweiter Spieler
        var zweiterSpielerVerfuegbar = verfuegbar.Where(s => s != ersterSpieler).ToList();
        wahrscheinlichkeiten = new();
        foreach (var spieler in zweiterSpielerVerfuegbar)
        {
            var spielwahrscheinlichkeit = 100;
            if (alleSpiele.Count > 0)
            {
                var vorherigesSpiel = alleSpiele[^1];
                if (vorherigesSpiel.Spieler.Contains(spieler))
                {
                    spielwahrscheinlichkeit -= 20;
                }

                if (alleSpiele.Count > 1)
                {
                    var vorvorherigesSpiel = alleSpiele[^2];
                    if (vorvorherigesSpiel.Spieler.Contains(spieler))
                    {
                        spielwahrscheinlichkeit -= 10;
                    }
                    for (int i = 0; i < alleSpiele.Count - 2; i++)
                    {
                        if (alleSpiele[i].Modus == Spielmodus.Doppel && alleSpiele[i].Spieler.Contains(spieler))
                        {
                            spielwahrscheinlichkeit -= 5;
                        }
                    }
                }

                if (alleSpiele.Any(spiel => spiel.Modus == Spielmodus.Doppel && spiel.Spieler.Contains(spieler) && spiel.Spieler.Contains(ersterSpieler)))
                {
                    spielwahrscheinlichkeit -= 40;
                }
            }

            if(spieler.Position == ersterSpieler.Position)
            {
                spielwahrscheinlichkeit -= 30;
            }

            if (!fuerZweiteHalbzeit && spieler.WillSchnellWeg)
            {
                spielwahrscheinlichkeit += 30;
            }

            if(spieler.BevorzugteMitspieler.Contains(ersterSpieler.Name) || ersterSpieler.BevorzugteMitspieler.Contains(spieler.Name))
            {
                spielwahrscheinlichkeit += 30;
            }

            if (HatVorigesDoppelGespielt(spieler))
            {
                spielwahrscheinlichkeit = 1;
            }

            if(HabenGemeinsamesDoppel(spieler, ersterSpieler))
            {
                spielwahrscheinlichkeit = 0;
            }

            wahrscheinlichkeiten.Add(spieler, spielwahrscheinlichkeit);
        }
        logger.LogDebug("Wahrscheinlichkeiten: {Spieler}", string.Join(", ", wahrscheinlichkeiten.Select(s => $"{s.Key.Name} ({s.Value})")));

        var zweiterSpieler = SelectPlayer(wahrscheinlichkeiten, zweiterSpielerVerfuegbar);

        if (HabenGemeinsamesDoppel(ersterSpieler, zweiterSpieler))
        {
            throw new InvalidSetupException(alleSpiele, Spielmodus.Doppel, [ersterSpieler, zweiterSpieler], "Die letzten verfügbaren Spieler haben bereits ein Doppel gespielt.");
        }

        logger.LogInformation("D {ErsterSpieler}, {ZweiterSpieler}", ersterSpieler.Name, zweiterSpieler.Name);
        halbzeit.Add(new(Spielmodus.Doppel, [ersterSpieler, zweiterSpieler], new(0, 0)));
        UpdatePlayerCount(ersterSpieler, anzahlDoppel, verfuegbar);
        UpdatePlayerCount(zweiterSpieler, anzahlDoppel, verfuegbar);
        logger.LogDebug("Jetzt noch übrig: {Spieler}", string.Join(", ", verfuegbar.Select(s => s.Name)));
    }

    private Player SelectPlayer(IReadOnlyDictionary<Player, int> probabilities, IEnumerable<Player> availablePlayers)
    {
        var random = new Random();
        var remaining = availablePlayers.ToList();
        int safetyCounter = 0;

        // ReSharper disable once SimplifyLinqExpressionUseAny // .All () is used to emphasize that all players have the same probability
        while (remaining.Count > 1 && !remaining.All(sp => probabilities[sp] == 1))
        {
            var rolls = remaining.ToDictionary(s => s, s => random.Next(probabilities[s] + 1));
            var highestRoll = rolls.Max(w => w.Value);
            remaining = remaining.Where(s => rolls[s] == highestRoll).ToList();
            logger.LogDebug("Remaining: {Players}", string.Join(", ", remaining.Select(s => $"{s.Name} ({rolls[s]})")));
            safetyCounter++;
            if (safetyCounter > 100)
            {
                throw new InvalidOperationException("Über 100 Mal ausgewürfelt, hier war wohl eine Endlosschleife. " + Environment.NewLine +
                                                    "Übrig: " + string.Join(", ", remaining.Select(s => $"{s.Name} ({probabilities[s]})")));
            }
        }
        return remaining[random.Next(remaining.Count)];
    }

    /// <summary>
    /// Aktualisiert die Spiele eines Spielers und entfernt ihn aus der Liste der verfügbaren Spieler, wenn er beide Spiele hat.
    /// </summary>
    /// <param name="player">Der Spieler, dessen Spielanzahl aktualisiert werden soll.</param>
    /// <param name="playCount">Ein Dictionary mit der aktuellen Anzahl der von allen Spielern bereits gespielten Spiele.</param>
    /// <param name="availablePlayers">Die Menge der noch für Spiele verfügbaren Spieler.</param>
    private void UpdatePlayerCount(Player player, Dictionary<Player, int> playCount, HashSet<Player> availablePlayers)
    {
        playCount[player] = playCount.GetValueOrDefault(player, 0) + 1;
        if (playCount[player] == 2)
        {
            availablePlayers.Remove(player);
            logger.LogDebug("Spieler hat beide Spiele: {Name}", player.Name);
        }
    }
}
