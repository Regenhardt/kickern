﻿namespace Spielplangenerator.Spielplan;

public class Spiel(Spielmodus modus, List<Player> spieler, Ergebnis ergebnis)
{
    public Spielmodus Modus { get; } = modus;
    public List<Player> Spieler { get; } = spieler;
    public Ergebnis Ergebnis { get; } = ergebnis;

    public string ToShortString() => Modus.ToString()[0] + " " + string.Join(", ", Spieler.Select(s => s.Name));
}
